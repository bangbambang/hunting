mapdata = {
    "width": 8,
    "height": 6,
    "path": [
        (1, 2), (1, 3), (1, 4),
        (2, 2), (2, 4),
        (3, 1), (3, 2), (3, 4),
        (4, 1), (4, 4),
        (5, 1), (5, 2), (5, 3), (5, 4),
        (6, 1), (6, 3), (6, 4)
    ],
    "start": (1,1)
}

def solve(path: list, start: tuple) -> list:
    startx, starty = start
    steps = []
    # step1: move northward
    # we only consider spots that allow 2nd step in our list
    steps.append([
        (x,y) for (x,y) in path if 
        # x is unchanged from last step
        x == startx
        # coordinates is valid for next movement
        and (x+1, y) in path
    ])

    # step2: move eastward along x
    # we consider constraint from previous step and next step
    steps.append([
        (x,y) for (x,y) in path if 
        # y is unchanged from last step
        y in set([y for (x,y) in steps[-1]])
        # x is changed by at least 1
        and x > startx
        # coordinates is valid for next movement
        and (x, y-1) in path
    ])

    # step 3: move southward along y
    # since it's the last step, we only use inference 
    # from previously known constraint (move north and east)
    steps.append([
        (x,y) for (x,y) in path if 
        # x is unchanged from last step
        x in set([x for (x,y) in steps[-1]])
        # coordinates is changed by at least -1
        and y < max(set([y for (x,y) in steps[-1]]))
        # coordinates is valid to complete movement
        and (x, y+1) in path 
    ])

    return steps

def generate_grid(width: int, height: int, reverse: bool = True) -> list:
    grid = [[(x,y) for x in range(width)] for y in range(height)]
    if reverse:
        # reverse is necessary to draw the map from top to bottom
        grid.reverse()
    return grid

def tokenize(coord: tuple) -> str:
    if coord == mapdata["start"]:
        token = "x"
    elif coord in mapdata["treasure"]:
        token = "$"
    elif coord in mapdata["path"]:
        token = "."
    else:
        token ="#"
    return token

def process():
    mapdata["steps"] = solve(mapdata["path"], mapdata["start"])

    mapdata["treasure"] = mapdata["steps"][-1]

    # print coordinates
    print("\npossible treasure coordinate(s) are as follows:")
    print(mapdata["treasure"])
    print("\nnote: (0,0) located at bottom left, you start at", str(mapdata["start"]),"\n")
    # print the marked map
    for x in generate_grid(mapdata["width"], mapdata["height"]):
        print("".join(map(tokenize, x)))
    # print map legend
    print("""\nlegend:
    - # : obstacle
    - . : pathway
    - x : starting position
    - $ : possible treasure position(s)""")

if __name__ == "__main__":
    process()