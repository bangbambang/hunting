# README #

Find possible treasure location using these ruleset:

- treasure is hidden within one of the clear path points, and the user must find it.
- From the starting position, the user must navigate in a specific order:

    - Up/North A step(s), then
    - Right/East B step(s), then
    - Down/South C step(s).
- output a list of probable coordinate points where the treasure might be located.
- Display the grid with all the probable treasure locations marked with a $ symbol.

### Requirements ###

* Python 3.5

### Running ###

execute `python hunt.py`

### Extend/ Repurpose ###

Modify `mapdata` dict:

- define `width` and `height` of the square grid (including walls)
- define traversable coordinates in `path`
- define `start`ing point

To apply custom navigation rules, modify solver (`solve(path: list, start: tuple) -> list`).
The method must return final coordinates and should not directly read or modify global state.